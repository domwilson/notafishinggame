﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedStrip : MonoBehaviour
{
    public float speed = 100f;
    float yRotation;
    float xComponent;
    float zComponent;
    public Vector3 boost;

    GameObject ball;
    // Start is called before the first frame update
    void Start()
    {
        yRotation = this.transform.rotation.eulerAngles.y;
        zComponent = Mathf.Cos(Mathf.Deg2Rad * yRotation) * speed;
        xComponent = Mathf.Sin(Mathf.Deg2Rad * yRotation) * speed;
        boost = new Vector3 (xComponent, 0, zComponent);
        ball = GameObject.Find("Sphere");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void speedBall()
    {
        ball.GetComponent<Rigidbody>().AddForce(boost);
    }
}
