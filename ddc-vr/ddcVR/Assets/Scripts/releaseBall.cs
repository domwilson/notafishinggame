﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class releaseBall : MonoBehaviour
{
    const double SPEED_STRIP_EFFECT = 5;
    Transform ball;
    Rigidbody ballRB;
    Vector3 BallSpeed;
    float correctiveForce = 10;
    bool hasSpeed = false;
    bool hasHitLane = false;
    double speedTime = 0;

    //Particle Effects
    public GameObject speedTrail;
    public GameObject pinImpact;
    public GameObject ballHitsObstacle;
    public GameObject obstacleHitsBall;

    bool hasTrail = false;
    bool hasExplosion = false;

    GameObject newTrail;

    AudioSource audio;

    public GameObject getNewTrail()
    {
        return newTrail;
    }
    // Start is called before the first frame update
    void Start()
    {
        BallSpeed = new Vector3(0,0,100);
        ballRB = this.GetComponent<Rigidbody>();
        audio = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    if(Time.time < 4)
        {
            //ballRB.AddForce(BallSpeed);
        }
     if(hasSpeed)
        {
            if(Time.time - speedTime > SPEED_STRIP_EFFECT)
            {
                hasSpeed = false;
                Debug.Log("Disabled speed strip effect");
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Pin" && hasSpeed && !hasExplosion)
        {
            hasExplosion = true;
            Debug.Log("BOOM");
            GameObject newExplosion = Instantiate(pinImpact);
            newExplosion.transform.position = this.transform.position;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Pin" && hasSpeed)
        {
            Debug.Log("BOOM");
            GameObject newExplosion = Instantiate(pinImpact);
            newExplosion.transform.position = this.transform.position;
        }
        if (collision.gameObject.tag == "Lane")
        {
            audio.Play();
            hasHitLane = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "speedStrip")
        {
            newTrail = Instantiate(speedTrail);
            newTrail.transform.position = this.transform.position;
        }
        if (other.gameObject.tag == "obstacle")
        {
            if (hasSpeed)
            {
                other.gameObject.SetActive(false);
                hasSpeed = false;
                //Mini-Boom
                GameObject newMiniExplosion = Instantiate(ballHitsObstacle);
                newMiniExplosion.transform.position = this.transform.position;
            }
            else
            {
                this.gameObject.SetActive(false);
                GameObject dust = Instantiate(obstacleHitsBall);
                dust.transform.position = this.transform.position;
                //Whomp Whomp Whomp
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "speedStrip")
        {
            other.GetComponent<speedStrip>().speedBall();
            hasSpeed = true;
            speedTime = Time.time;
        }
    }
}
