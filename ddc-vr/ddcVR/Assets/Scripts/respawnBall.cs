﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respawnBall : MonoBehaviour
{
    GameObject ball;
    Vector3 ballOffset;

    //Particle Effects
    public GameObject ballResurrection;
    // Start is called before the first frame update
    void Start()
    {
        ball = GameObject.Find("Sphere");
        ballOffset = new Vector3(0, 1, 0);

    }

    // Update is called once per frame
    void Update()
    {
        if(!ball.activeSelf || ball.transform.position.y < -1)
        {
            GameObject newBallEffect = Instantiate(ballResurrection);
            ball.SetActive(true);
            ball.transform.position = this.transform.position + ballOffset;
            newBallEffect.transform.position = ball.transform.position;
            ball.GetComponent<Rigidbody>().freezeRotation = true;
            ball.GetComponent<Rigidbody>().velocity = new Vector3 (0,0,0);
            Destroy(ball.GetComponent<releaseBall>().getNewTrail());
        }
        else
        {
            ball.GetComponent<Rigidbody>().freezeRotation = false;
        }
    }
}
