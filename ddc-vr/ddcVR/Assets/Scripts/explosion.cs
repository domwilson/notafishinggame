﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosion : MonoBehaviour
{
    GameObject ball;
    public float power = 1.0f;
    public float radius = 5f;
    public float upForce = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        Collider[] colliders = Physics.OverlapSphere(this.transform.position, radius);
        foreach (Collider hit in colliders)
        {
            if (hit.gameObject.tag != "Ball" && hit.GetComponent<Rigidbody>() != null)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                rb.AddExplosionForce(power, this.transform.position, radius, upForce, ForceMode.Impulse);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
