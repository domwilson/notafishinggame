﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killSelf : MonoBehaviour
{
    public double killTimer = 5;
    double instantiatedTime;
    // Start is called before the first frame update
    void Start()
    {
        instantiatedTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - instantiatedTime > killTimer)
        {
            Destroy(this.gameObject);
        }
    }
}
