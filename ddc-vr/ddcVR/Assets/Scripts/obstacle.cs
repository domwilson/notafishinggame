﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class obstacle : MonoBehaviour
{
    public float rightSpeed = 1f;
    public float leftSpeed = -1f;
    Vector3 obstacleSpeed;
    Vector3 reverseSpeed;
    bool reverse = false;
    // Start is called before the first frame update
    void Start()
    {
        obstacleSpeed = new Vector3(rightSpeed, 0, 0);
        reverseSpeed = new Vector3(leftSpeed, 0, 0);

    }

    // Update is called once per frame
    void Update()
    {
        if(reverse)
        {
            this.GetComponent<Rigidbody>().AddForce(reverseSpeed);
        }
        else
        {
            this.GetComponent<Rigidbody>().AddForce(obstacleSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "walls")
        {
            reverse = !reverse;
        }
    }
}
