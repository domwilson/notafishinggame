﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour
{
    GameObject ball;
    // Start is called before the first frame update
    void Start()
    {
        ball = GameObject.Find("Sphere");
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = ball.transform.position;
    }
}
